variable "prefix" {
  type        = string
  default     = "raad"
  description = "recipe-app-api-devops"
}
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  type        = string
  default     = "abray497@gmail.com"
  description = "contact of project manager"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}


variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "012812551186.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops"
}

variable "ecr_image_proxy" {
  default     = "012812551186.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy"
  description = "ECR image for proxy"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "allan-bray.ca"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
